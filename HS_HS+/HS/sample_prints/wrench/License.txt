Wrench19mmx25mm_Closed_Small by Dave Robertson for Aleph Objects, Inc is licensed under the Creative Commons - Attribution - Share Alike v4.0 International license.

http://creativecommons.org/licenses/by-sa/4.0/

LulzBot is a Registered Trademark of Aleph Objects, Inc.
